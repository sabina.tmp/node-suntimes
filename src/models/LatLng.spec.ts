import { expect } from 'chai';
import LatLng, { randomLatLng } from './LatLng';

describe('randomLatLng', function () {
  it('should have correct properties', function () {

    const object = randomLatLng();

    expect(object).to.have.property('lat');
    expect(object.lat).to.be.a('number');
    expect(object.lat).to.be.above(-90);
    expect(object.lat).to.be.below(90);


    expect(object).to.have.property('lng');
    expect(object.lng).to.be.a('number');
    expect(object.lng).to.be.above(-180);
    expect(object.lng).to.be.below(180);
  });
});