export const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

export const asyncForEach = async (array: string | any[], callback: { (chunk: any): Promise<void>; (arg0: any, arg1: number, arg2: any): any; }) => {
    for (let index = 0; index < array.length; index++) {
        try {
            await callback(array[index], index, array);
        } catch (e) {console.error(e);}
    }
};

export const between = (min: number, max: number): number => {
    const number = (Math.random() * (max - min) + min);

    return Number(number.toFixed(7));
};
