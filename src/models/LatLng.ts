import { between } from '../utils';


export default interface LatLng {
    lat: number;
    lng: number;
};

export const randomLatLng = (): LatLng => {
    return {
        lat: between(-90, 90),
        lng: between(-180, 180)
    }
};