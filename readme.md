# Coding example

## Requirements

- Generate a list of 100 random lat / long points around the world
- Using https://sunrise-sunset.org/api, fetch the sunrise / sunset times
- Run 5 requests in parrallel, run next 5 after 5 second break if they've all finished
- Find earliest sunrise and list the day length for this time
- Wrap api into a class
- Now assume the calling code will be getting lots of different data from different services about each point (sunrise times just being one).

#### Bonus points:
- Install and use ramda, implement some functional programming
- Use ES6/7 array features
- Use async / await
- Handle errors properly
- Additional functionality
- Unit tests

## Start server for prod
`npm run prod`

## Start server for dev
`npm run dev`

## Run tests
`npm run test`