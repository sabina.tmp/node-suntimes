"use strict";

import LatLng, {randomLatLng } from './models/LatLng';
import Suntime from './models/Suntime';
import Suntimes from './services/Suntimes';
import express from 'express';
// Set up the express app
const app = express();

// Generate a list of 100 random lat / long points around the world
let points: LatLng[] = [];
// todo: increase this to 100
for (let i = 0; i < 10; i++) {
    points.push(randomLatLng());
}

const suntimesApi = new Suntimes('https://api.sunrise-sunset.org/json?lat=%latPlaceholder%&lng=%lngPlaceholder%');

let suntimes : Suntime[] = [];
console.log('Please wait. Loading all suntimes, server will start after they finish');
suntimesApi.getAllTimes(points).then((results) => {
    // get all suntimes before we start the server.
    // suntimes don't really change
    suntimes = results;

    // we've loaded the times, start the server
    const PORT = 5000;
    app.listen(PORT, () => {
        console.log(`server running on port ${PORT}`)
    });

}).catch(e => console.log(e));

// add endpoint for earliest sunrise
app.get('/sunrise/earliest', (req: any, res: any) => {
    // Find earliest sunrise and list the day length for this time
    const earliestSunriseDay = suntimesApi.earliestSunriseDay(suntimes);

    res.status(200).send({
      success: 'true',
      dayLength: earliestSunriseDay.day_length
    })
});

