import LatLng from '../models/LatLng';
import Suntime from '../models/Suntime';
import R from 'ramda';
import axios from 'axios';
import { delay, asyncForEach } from '../utils';

export default class Suntimes {
    private url: string;
    constructor(url: string) {
        this.url = url;
    }

    public getLocationTimes(latLng: LatLng) {
        const url = this.getUrl(latLng);
        return axios.get(url);
    };

    /**
     *  Run 5 requests in parrallel, run next 5 after 5 second break if they've all finished
     *
     * @param points
     */
    public getAllTimes(points: LatLng[]): Promise<any> {
        return new Promise(async (resolve, reject) => {
            let suntimes: Suntime[] = [];

            const chunks = R.splitEvery(5, points);
            for(let chunk of chunks) {
                try {
                    console.log('Loading chunk: ', chunk);

                    let promises: any[] = [];
                    R.forEach((point: LatLng) => {
                        promises.push(this.getLocationTimes(point));
                    }, chunk);

                    const responses = await Promise.all(promises);
                    // todo: handle case when r.data.status !== 'OK'
                    const times = R.map((r: any) => r.data.results, responses);

                    suntimes = R.concat(suntimes, times);

                    // todo: rewrite this so we don't wait after everything has loaded
                    await delay(5000);
                } catch(e) {
                    reject(e);
                }
            }

            resolve(suntimes);
        });
    }

    public earliestSunriseDay(suntimes: Suntime[]):Suntime {
        return R.reduce((a: Suntime, b: Suntime) => {
            // return
            const dateA = new Date(a.sunrise);
            const dateB = new Date(b.sunrise);

            return dateA > dateB ? a : b;
        }, suntimes[0], suntimes);
    }

    private getUrl(latLng: LatLng): string {
        return R.replace('%lngPlaceholder%', Number(latLng.lng).toString(),
            R.replace('%latPlaceholder%', Number(latLng.lat).toString(), this.url)
        );
    }
}